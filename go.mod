module gitlab.com/nkorovikov/dummy-proto-import

go 1.21.4

require (
	gitlab.com/nkorovikov/dummy-proto v0.0.0-20240113213407-e2f3152d10aa
	google.golang.org/protobuf v1.32.0
)
