package main

import (
	"fmt"
	"gitlab.com/nkorovikov/dummy-proto-import/pkg/api/dummy_import"
	"gitlab.com/nkorovikov/dummy-proto/pkg/api/dummy"
)

func main() {
	pw := &dummy_import.PersonWrap{
		Id: 1,
		Person: &dummy.Person{
			Name:          "111",
			Id:            2,
			HasPonycopter: false,
		},
	}
	p := dummy.Person{
		Name:          "111",
		Id:            2,
		HasPonycopter: false,
	}

	fmt.Println(pw, p)
}
