proto-gen:
	protoc -I . -I proto-vendor --go_out=pkg --go_opt=paths=source_relative \
        --go-grpc_out=pkg --go-grpc_opt=paths=source_relative \
        api/dummy_import/dummy_import.proto